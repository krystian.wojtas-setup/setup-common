#!/bin/bash

# Stop on first failure
set -e

# Show each command
set -x

ssh_port_forward()
{
    # Get arguments
    source_port="$1"
    destination_ip="$2"
    destination_port="$3"

    ssh_forward_command="
        ssh \
            `# Do not execute remote command and only forward port` \
            -N \
            \
            `# Bind to local interface to disallow access this tunnel from external hosts` \
            -L \
                localhost:$source_port:$destination_ip:$destination_port \
            \
            `# Alias to machine to connect to` \
            v
    "

    # Get rid of extra spaces by not quoting $ssh_forward_command variable
    ssh_forward_command="$(echo $ssh_forward_command)"

    # Kill any other instance of the same forwarding command
    pkill \
        --full \
        --exact \
        "$ssh_forward_command" \
    ||
    true \

    # Run forward command
    $ssh_forward_command
}

# Get server number argument
server="$1"

# Ensure that server number is passed
if test -z "$server"; then
    echo >&2 "ERROR: Usage: $0 <server number>"
    exit 1
fi

# Use subshell to not polute parent shell with secrets
(
    # Decrypt configuration for all servers
    secret_all_servers="$(pass show motorola/net-ssh-tun-motorola)"

    # Use intermediate variable to fail script if pass does not have this variable
    . <(echo "$secret_all_servers")

    # Get details of specified server
    eval server_source_port="\${server_${server}_source_port}"
    eval server_destination_ip="\${server_${server}_destination_ip}"
    eval server_destination_port="\${server_${server}_destination_port}"

    # Forward port for particular server
    ssh_port_forward \
        "$server_source_port" \
        "$server_destination_ip" \
        "$server_destination_port" \
)
