;;
;; Install package from command line. Example:
;;
;;   $ emacs --batch --expr \"(define pkg-to-install 'smex)\" -l emacs-pkg-install.el
;;

;; Maybe it would be usefull
;; (setq url-proxy-services
;;    '(("no_proxy" . "^\\(localhost\\|10.*\\)")
;;      ("http" . "proxy.com:8080")
;;      ("https" . "proxy.com:8080")))


;; load emacs 24's package system. Add MELPA repository.
(when (>= emacs-major-version 24)
  (require 'package)

  (add-to-list 'package-archives
    '("melpa" . "https://melpa.org/packages/"))
)

(package-initialize)

(package-refresh-contents)

(package-install pkg-to-install)
