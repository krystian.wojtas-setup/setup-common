; turn off welcome screen
(setq inhibit-startup-screen t)
; turn off help message
(setq initial-scratch-message nil)

; do not use tabs
(setq-default indent-tabs-mode nil)

; do not show menubar
(menu-bar-mode -1)
; do not show toolbar
(tool-bar-mode -1)

; turn off blinking cursor in x
(blink-cursor-mode 0)

; turn off blinkin cursor in console
(setq visible-cursor nil)

; wrap lines
(global-visual-line-mode t)

; create directory to store backups if not exist yet
(progn
   (if (not (file-exists-p "~/.emacs.d/backups"))
	    (make-directory "~/.emacs.d/backups" t) nil))

; backups settings
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.emacs.d/backups"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups

; remove trailing whitespace
(add-hook 'before-save-hook 'delete-trailing-whitespace)

; answer y or n instead of full yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

; scroll one line
(global-set-key (kbd "M-n") `scroll-up-line)
(global-set-key (kbd "M-p") `scroll-down-line)

; Increase watched files to avoid warning
(setq lsp-file-watch-threshold 5000)

; Use system clipboards
(setq x-select-enable-clipboard t
    x-select-enable-primary t)

(custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   ;; https://magit.vc/manual/magit/Theming-Faces.html
   ;; '(magit-diff-added ((t (:background "black" :foreground "green3"))))
   '(magit-diff-added ((t (:foreground "green3"))))
   '(magit-diff-removed ((t (:foreground "red3")))))


; Set global keybinding for magit status
(global-set-key (kbd "C-x g") 'magit-status)

; Define internal gitlab instance
                                        ; https://logc.github.io/blog/2019/08/23/setting-up-magit-forge-with-github-enterprise-server/
; It cannot be run directly on initialization as forge-alist is not yet defined
; It should be run when forge is initialized
 ;; (push '("domain.com" "domain.com/api/v4"
 ;;         "domain.com" forge-gitlab-repository)
 ;;         forge-alist)

(add-hook 'sh-mode-hook (lambda () (setq tab-width 4 sh-basic-offset 4 indent-tabs-mode t)))
