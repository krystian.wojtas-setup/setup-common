;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "monospace" :size 14))
;; (setq doom-font (font-spec :family "monospace" :size 22))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one-light)
;; w terminalu na 256 kolorach zle sie wyswietlaja dopelnienia nazw katalogow company?
;; (setq doom-theme 'doom-one-light)
;; w terminalu na 256 kolorach zle sie wyswietla szukany tekst przez isearch ctrl+c
;; (setq doom-theme 'doom-opera) ;;
;; w terminalu na 256 kolorach zle sie wyswietla podkreslenie wybranego bufora w ctrl+b
(setq doom-theme 'doom-Iosvkem)
(custom-set-faces!
  ;; selected text on black background
  `(region :background ,"#050505")
  '(magit-diff-added  :foreground "green3")
  '(magit-diff-removed :foreground "red3")
)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;; Paths to projects
;; with . <level> it looks for subdirectories
(setq projectile-project-search-path `(~/work/rust/ (~/project/ . 1)))
;; some projects may be opened in previous emacs sessions
;; then projectile keeps them persistent
;; to see all opened projects
;; projecttile-project-info
;; to clean all opened projects
;; projecttile-clear-known-projects

;; Add gitlab instance address
;; It will use ~/.authinfo.gpg by default
;; It should contain line:
;; machine gitlab.domain..com/api/v4 login Krystian.Wojtas^forge password <TOKEN>
;; TOKEN should have all accesses:
;; - api
;; - read_user
;; - read_repository
;; - write_repository
;; - read_registry
;; To encrypt file
;; cat .authinfo | gpg --output .authinfo.gpg -e --recipient deadb17/legion
(after! forge
 (push '("gitlab.domain.com" "gitlab.domain.com/api/v4"
         "gitlab.domain.com" forge-gitlab-repository)
         forge-alist)
)

;; Gitlab authentication is in local password store
;; Does not work yet
;; https://github.com/magit/forge/issues/172
;; (setq auth-sources `(password-store))

;; configuration from
;; https://robert.kra.hn/posts/rust-emacs-setup/
;;
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; rustic = basic rust-mode + additions

(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status)
              ("C-c C-c e" . lsp-rust-analyzer-expand-macro)
              ("C-c C-c d" . dap-hydra)
              ("C-c C-c h" . lsp-ui-doc-glance))
  :config
  ;; uncomment for less flashiness
  ;; (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  (setq rustic-format-on-save t)
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm, but don't try to
  ;; save rust buffers that are not file visiting. Once
  ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
  ;; no longer be necessary.
  (when buffer-file-name
    (setq-local buffer-save-without-query t)))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; for rust-analyzer integration

(use-package lsp-mode
  :ensure
  :commands lsp
  :custom
  (lsp-auto-guess-root t)
  (lsp-inlay-hint-enable t)
  ;; what to use when checking on-save. "check" is default, I prefer clippy
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all nil)
  (lsp-idle-delay 0.6)
  ;; This controls the overlays that display type and other hints inline. Enable
  ;; / disable as you prefer. Well require a `lsp-workspace-restart' to have an
  ;; effect on open projects.
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-inlay-hints-mode t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints nil)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints nil)
  (lsp-rust-analyzer-display-reborrow-hints nil)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))

(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-peek-show-directory t)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-doc-enable nil))

;; disable global word-wrap in this mode
;; (add-to-list '+word-wrap-disabled-modes 'emacs-fundamental-mode)

; Load common configuration shared with emacs without doom
(load-file "~/.emacs-default/init.el")
