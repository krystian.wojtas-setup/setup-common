# History settings
HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000

# Ignore duplicated commands history list
setopt hist_ignore_dups
# Ignore commands that start with space
setopt hist_ignore_space
# Append history to the history file (no overwriting)
setopt appendhistory
# Do not query the user before executing rm * or rm path/*.
setopt rmstarsilent

# Search for command in history containing current buffer
# alt + p
bindkey "^[p" history-beginning-search-backward
# alt + n
bindkey "^[n" history-beginning-search-forward

# zstyle :compinstall filename '/home/k/.zshrc'

# Show completion description
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*' menu select

# Highlight the current autocomplete option
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

# # Better SSH/Rsync/SCP Autocomplete
# zstyle ':completion:*:(scp|rsync):*' tag-order ' hosts:-ipaddr:ip\ address hosts:-host:host files'
# zstyle ':completion:*:(ssh|scp|rsync):*:hosts-host' ignored-patterns '*(.|:)*' loopback ip6-loopback localhost ip6-localhost broadcasthost
# zstyle ':completion:*:(ssh|scp|rsync):*:hosts-ipaddr' ignored-patterns '^(<->.<->.<->.<->|(|::)([[:xdigit:].]##:(#c,2))##(|%*))' '127.0.0.<->' '255.255.255.255' '::1' 'fe80::*'

# # Allow for autocomplete to be case insensitive
# zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' \
#   '+l:|?=** r:|?=**'

# Enable bash completion
autoload bashcompinit
bashcompinit

# Initialize the autocompletion
autoload -Uz compinit && compinit -i

# Emacs style key bindings
bindkey -e

# Jump between words like in bash
autoload -U select-word-style
select-word-style bash

# Replace typed ..// by ../../
rationalise-slash() {
  if [[ $LBUFFER = *../ ]]; then
    LBUFFER+=../
  else
    LBUFFER+=/
  fi
}
zle -N rationalise-slash
bindkey / rationalise-slash

# Make ctrl+u to kill line from cursor to beginning instead of whole line
bindkey \^U backward-kill-line

# Allow comments in interactive session
setopt interactivecomments

# If pattern maching fails, then do not complain and provide chars as it is to command
unsetopt nomatch

# Do not use extended glob pattern matching
# unsetopt extendedglob

# Time builtin format
# TIMEFMT=$'\nreal\t%E\nuser\t%U\nsys\t%S'
# Disable builtin time, use system binary
disable -r time

# Path to your oh-my-zsh installation.
# export ZSH="$HOME/src/oh-my-zsh"
# plugins=(zsh-autosuggestions)
# source $ZSH/oh-my-zsh.sh

# Init autosuggestion plugin
if test -r "$HOME"/local/zsh-autosuggestions/src/zsh-autosuggestions.zsh; then
    source "$HOME"/local/zsh-autosuggestions/src/zsh-autosuggestions.zsh
fi

# Configure color of suggestion
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=6"

autosuggest-keep() {

    # Save current cursor location
    local saved_cursor="$CURSOR"

    # Accept suggestion which moves cursor to the end
    zle autosuggest-accept

    # Move cursor to previous place
    CURSOR="$saved_cursor"
}
# Install custom widget
zle -N autosuggest-keep
# alt + j
bindkey '^[j' autosuggest-keep


setup_prompt_zsh() {

    # Declare empty function for possible overwrite later
    git_super_status() {
        true
    }

    # Overwrite empty function with proper implementation if repository is initialized
    if test -f "$HOME/local/zsh-git-prompt/src/zshrc.sh"; then
        source "$HOME/local/zsh-git-prompt/src/zshrc.sh"
    fi

    # Command prompt
    PROMPT='%F{2}%n@%m%f %B%?%b %F{4}%*%f %F{3}%~%f $(git_super_status)
%# '

}

# Source kubectl completion if installed
if command -v kubectl >/dev/null 2>&1; then
    source <(kubectl completion zsh)
fi

setup_prompt() {

    if command -v starship >/dev/null 2>&1; then
        eval "$(starship init zsh)"
    else
        setup_prompt_zsh
    fi

}

setup_prompt

# Source common configuration for bash and zsh
test -r "$HOME/.shell-setup-common" && source "$HOME/.shell-setup-common"
