(
("default" . ((user-emacs-directory . "~/.emacs-default")))
("doom" . ((user-emacs-directory . "~/.emacs-doom")
           (env . (("DOOMDIR" . "~/.doom.d")))))
)
