# History settings
HISTSIZE=100000
HISTFILESIZE=100000

# If console is pseudoterminal in X, then use colors.
# It is assummed white background
# if ! fgconsole 2>/dev/null; then
#     PS1=""
#     PS1="$PS1\[\033[38;5;22m\]\u@\h" # user @ host
#     PS1="$PS1\[\033[38;5;88m\] \$?"  # exit code of last command
#     PS1="$PS1\[\033[38;5;57m\] \t"   # timestamp
#     PS1="$PS1\[\033[38;5;25m\] \w"   # work dir
#     PS1="$PS1\[\033[38;5;0m\]\n\$ "  # prompt in new line
#     export PS1
# fi
# No colors
PS1=""
PS1="$PS1u@\h" # user @ host
PS1="$PS1 \$?"  # exit code of last command
PS1="$PS1 \t"   # timestamp
PS1="$PS1 \w"   # work dir
PS1="$PS1\n\$ "  # prompt in new line
export PS1

# git and g alias completion
if [ -e "/usr/share/bash-completion/completions/git" ]; then
    source /usr/share/bash-completion/completions/git
    __git_complete g __git_main
fi

# tig and i alias completion
if [ -e /usr/share/bash-completion/completions/tig ]; then
    source /usr/share/bash-completion/completions/tig
    complete -F _tig i
fi

# ssh and s alias completion
if [ -e "/usr/share/bash-completion/completions/ssh" ]; then
    source /usr/share/bash-completion/completions/ssh 2>/dev/null
    complete -F _ssh s
fi

# Source common configuration for bash and zsh
test -r "$HOME/.shell-setup-common" && source "$HOME/.shell-setup-common"
