## Spacemacs

Spacecemacs introduce few workarounds
- It has conflict with custom ~/.emacs file.
  Loading spacemacs conditionally from ~/.emacs does not work this way
  ```
  ;; load spacemacs settings if installed
  (let ((personal-settings "~/.emacs.d/init.el"))
    (when (file-exists-p personal-settings)
      (load-file personal-settings))
    )
  ```
  So rename .emacs from this repo to .emacs_template and copy it manually if needed and spacemacs is not installed using command
  ```
  cp .emacs_template .emacs
  ```
- Elpa signatures expired. As workaround disable signature check by adding line to spacemacs config
  ```
  (setq package-check-signature nil)
  ```
